# Some nginx tips and tricks

## Navigation

* [Simple disable search engine indexing](/disallow-robots)
* [Many redirects by Nginx MAP](/redirect-map)
* [Different robots.txt and sitemap.xml for https and http versions](/https-unique-robots)
