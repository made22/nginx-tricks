[⇚ Return to main page](https://gitlab.com/made22/nginx-tricks)

# Nginx simple disable google indexing

Sometimes developers can delete or edit the robots.txt on dev/stage project's versions. After that search engine systems can
start to index these projects.

We can disallow indexing with nginx and ignore all manipulations with file robots.txt. Add this block to `server`
section in site's config:

```
location = /robots.txt {
    allow all;
    access_log off;
    log_not_found off;
    add_header Content-Type text/plain;
    return 200 "User-agent: *\nDisallow: /\n";
}
```

Check and restart nginx if the settings are correct:

```
    nginx -t
    service nginx restart
```

Also, we can create the file with this code and include in multiple configs.

Another simple way for disallow search engine systems it is using Http Basic Auth.
