[⇚ Return to main page](https://gitlab.com/made22/nginx-tricks)

# Different robots.txt and sitemap.xml for https and http versions

1. Create the files robots.txt and sitemap.xml with settings for http-version.
2. Create the files robots_https.txt and sitemap_https.xml with settings for https-version.
3. Add this block to `server` section (where ssl settings) in nginx config:

```
location /robots.txt {
    rewrite ^/robots.txt$ /robots_https.txt last;
}

location /sitemap.xml {
    rewrite ^/sitemap.xml$ /sitemap_https.xml last;
}
```

4. Check and restart nginx if the settings are correct:

```
    nginx -t
    service nginx restart
```
