[⇚ Return to main page](https://gitlab.com/made22/nginx-tricks)

# Many redirects by Nginx MAP

Sometimes we need to add hundreds or thousands redirects our project. Using the nginx `map` for redirects. It is the
best way to do this easily, correct and with better performance.

1. Go to `/etc/nginx` and create the file `redirect.map` (command `touch redirect.map`)
2. Open site's nginx config and add to the top this block:

```
    map_hash_bucket_size 128;
    map $request_uri $new_uri {
        default "";
        include /etc/nginx/redirect.map;
    }
```

Increase `map_hash_bucket_size` value if you have any errors (it can be for a large number of redirects).

3. Add to server section:

```
    if ($new_uri != "") {
        rewrite ^(.*)$ $new_uri permanent;
    }
```

4. Add to `redirect.map` your redirects in format:

```
    /uri_redirect_from /uri_redirect_to
```

Also, you can add regex:

```
    ~^/uri_redirect_from_regex? /uri_redirect_to;
```

5. Check and restart nginx if the settings are correct:

```
    nginx -t
    service nginx restart
```

## How it works

Nginx compares `$request_uri` with left part in each rows from `redirect.map`. And when left part equals `$request_uri`
nginx sets value for `$new_uri` from right part in this row. When `$new_uri` not empty nginx do redirect to `$new_uri`.
